FROM python:bullseye AS builder
USER root
WORKDIR /build
COPY . /build
RUN apt update && apt install -y upx-ucl
RUN pip3 install -r requirements.txt
RUN pyinstaller -s -F main.py
RUN mkdir -p -m 1777 /output/tmp
RUN cp -r static /output
RUN staticx /build/dist/main /output/main.sx ; chmod 755 /output/main.sx


FROM scratch
WORKDIR /
USER root
COPY --from=builder /output /
CMD ["/main.sx"]
EXPOSE 8080/tcp
USER 1001
