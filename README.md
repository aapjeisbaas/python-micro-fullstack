## run without docker
```
python main.py

INFO:     Will watch for changes in these directories: ['/home/svanbroekhoven/git/python-micro-fullstack']
INFO:     Uvicorn running on http://0.0.0.0:8080 (Press CTRL+C to quit)
INFO:     Started reloader process [114067] using StatReload
INFO:     Started server process [114069]
INFO:     Waiting for application startup.
INFO:     Application startup complete.
^CINFO:     Shutting down
INFO:     Waiting for application shutdown.
INFO:     Application shutdown complete.
INFO:     Finished server process [114069]
INFO:     Stopping reloader process [114067]
```

# Build and run with docker
```
docker build -t py-fullstack . && docker run -p 8080:8080  py-fullstack
```

# Run from remote registry
```
docker run -p 8080:8080 registry.gitlab.com/aapjeisbaas/python-micro-fullstack:latest
```
