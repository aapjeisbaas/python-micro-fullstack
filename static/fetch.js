function fetchData() {
    console.log("START FETCH")
    fetch("/api/uuid").then( response => {
        if (!response.ok) {
            throw Error("Fetch failed")
        }
        return response.json();
    }).then(data => {
        console.log(data);
        const html = data.map(value => {
            return `
            <tr>
                <td>${value.id}</td>
                <td>${value.uuid}</td>
            </tr>
            `;
        }).join("");
        document.getElementById("tbody").innerHTML = html;
    }).catch(error => {
        console.log(error);
    });
}

fetchData();