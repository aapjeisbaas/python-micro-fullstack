from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
import uuid

api_app = FastAPI(title="api app")


@api_app.get("/hello")
async def hello():
    return {"Hello": "World"}


@api_app.get("/uuid")
async def uuid_list():
    return [{"id": i, "uuid": str(uuid.uuid4())} for i in [*range(1, 10, 1)]]


app = FastAPI(title="main app")

app.mount("/api", api_app)
app.mount("/", StaticFiles(directory="static", html=True), name="ui")


if __name__ == "__main__":
    import uvicorn
    import main  # Without this import uvicorn can't find this module

    uvicorn.run("main:app", host="0.0.0.0", port=8080, log_level="info", reload=False)
